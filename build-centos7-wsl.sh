#! /bin/bash

set -e

# Update the package manager and upgrade the system
# #################################################
yum -y install epel-release
yum -y install https://repo.ius.io/ius-release-el7.rpm
yum -y update
yum -y install net-tools passwd bzip2 sudo wget which vim 
yum -y install tini supervisor
yum -y install openssh-server openssh-clients
yum -y install git236 tcl tk make
ssh-keygen -A

# Set the locale
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8


# Install icewm and tightvnc server.
# #################################################
yum -y install xorg-x11-server-Xorg xorg-x11-xauth xorg-x11-xinit
yum -y install xorg-x11-fonts*
yum -y install icewm
yum -y install xterm firefox
yum -y install tigervnc-server 
###yum clean all
/bin/dbus-uuidgen > /etc/machine-id


# install and setup noVNC
# #################################################
yum -y install python3 python3-numpy
pip3 install install websockify
mkdir -p /usr/share/novnc
###tar xvzf ./tgz/novnc.v1.3.0.tar.gz -C /usr/share/novnc
wget -qO- https://github.com/novnc/noVNC/archive/refs/tags/v1.3.0.tar.gz | tar xz --strip 1 -C /usr/share/novnc
mkdir -p /usr/share/novnc/utils/websockify
wget -qO- https://github.com/novnc/websockify/archive/refs/tags/v0.10.0.tar.gz | tar xz --strip 1 -C /usr/share/novnc/utils/websockify
###tar xvzf ./tgz/websockify.v0.10.0.tar.gz -C /usr/share/novnc/utils/websockify
ln -sf /usr/share/novnc/vnc_lite.html /usr/share/novnc/index.html


# Setup Supervisord
# #################################################
cp supervisord.conf /etc/supervisord.conf
cp supervisord.d.centos7/* /etc/supervisord.d/
cp supervisor.centos7.sh /root/supervisor.sh
chmod a+x /root/supervisor.sh
echo 'export DISPLAY=:1' >> /root/.bashrc

# Setup Gitlab
# #################################################
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" |  bash
yum -y install gitlab-runner
usermod -a -G docker gitlab-runner

# Set up User (centosuser)
# #################################################
if [[ ! -d "/home/centosuser" ]]; then
 useradd -s /bin/bash -m -b /home centosuser
 echo 'export DISPLAY=:1' >> /home/centosuser/.bashrc
fi
touch /home/centosuser/.Xauthority
chmod go-rwx /home/centosuser/.Xauthority
mkdir -p /home/centosuser/.vnc
chown -R centosuser:centosuser /home/centosuser

# Set up User centosadmin
# #################################################
if [[ ! -d "/home/centosadmin" ]]; then
 useradd -s /bin/bash -m -b /home centosadmin
 echo 'export DISPLAY=:1' >> /home/centosadmin/.bashrc
 echo "centosadmin  ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/centosadmin
 mkdir /home/centosadmin/.ssh
fi
cp authorized_keys /home/centosadmin/.ssh/authorized_keys
chown -R centosadmin:centosadmin /home/centosadmin/.ssh
chmod -R go-rwx /home/centosadmin/.ssh
cp wsl.conf /etc

