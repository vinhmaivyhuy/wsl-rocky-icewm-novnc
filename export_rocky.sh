#!/bin/bash
set -e

echo Docker process need to be started
docker pull rockylinux
docker container prune --force
docker run -t rockylinux date
dockerContainerID=$(docker container ls -a | grep -i rockylinux | awk '{print $1}')
docker export $dockerContainerID > /mnt/c/temp/rockylinux.tar
