#!/bin/bash
set -e

## Daemon
#/usr/bin/supervisord -c /etc/supervisord.conf

## Shell
#if [[ ! -d "/tmp/.X1-lock" ]]; then
export HOME=/home/rockyuser
export USER=rockyuser
cd /$HOME
exec /usr/bin/supervisord -n -c /etc/supervisord.conf
#fi
