#!/bin/bash
set -e

echo Docker process need to be started
docker pull centos:7
docker container prune --force
docker run -t centos:7 date
dockerContainerID=$(docker container ls -a | grep -i centos | awk '{print $1}')
docker export $dockerContainerID > /mnt/c/temp/centos7.tar
