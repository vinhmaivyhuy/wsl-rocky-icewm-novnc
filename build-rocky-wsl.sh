#! /bin/bash

set -e

# Update the package manager and upgrade the system
# #################################################
yum -y install epel-release
yum -y update
yum -y install net-tools passwd bzip2 sudo wget which vim 
yum -y install tini supervisor
yum -y install openssh-server openssh-clients
yum -y install langpacks-en glibc-langpack-en
yum -y install git tcl tk make
ssh-keygen -A

# Set the locale
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8


# Install icewm and tightvnc server.
# #################################################
yum -y install xorg-x11-server-Xorg xorg-x11-xauth xorg-x11-xinit
yum -y install xorg-x11-fonts*
yum -y install icewm
yum -y install xterm firefox
yum -y install tigervnc-server 
###yum clean all
/bin/dbus-uuidgen > /etc/machine-id


# install and setup noVNC
# #################################################
yum -y install python3.9 python39-numpy
alternatives --set python3 /usr/bin/python3.9
rpm -Uvh ./rpm/python3-websockify-0.10.0-1.el9.noarch.rpm
yum -y install novnc

# Setup Supervisord
# #################################################
cp supervisord.conf /etc/supervisord.conf
cp supervisord.d/* /etc/supervisord.d/
cp supervisor.sh /root/
chmod a+x /root/supervisor.sh
echo 'export DISPLAY=:1' >> /root/.bashrc

# Setup Gitlab
# #################################################
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" |  bash
yum -y install gitlab-runner
usermod -a -G docker gitlab-runner


# Set up User (rockyuser)
# #################################################
if [[ ! -d "/home/rockyuser" ]]; then
 useradd -s /bin/bash -m -b /home rockyuser
 echo 'export DISPLAY=:1' >> /home/rockyuser/.bashrc
fi
touch /home/rockyuser/.Xauthority
chmod go-rwx /home/rockyuser/.Xauthority
mkdir -p /home/rockyuser/.vnc
chown -R rockyuser:rockyuser /home/rockyuser

# Set up User rockyadmin
# #################################################
if [[ ! -d "/home/rockyadmin" ]]; then
 useradd -s /bin/bash -m -b /home rockyadmin
 echo 'export DISPLAY=:1' >> /home/rockyadmin/.bashrc
 echo "rockyadmin  ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/rockyadmin
 mkdir /home/rockyadmin/.ssh
fi
cp authorized_keys /home/rockyadmin/.ssh/authorized_keys
chown -R rockyadmin:rockyadmin /home/rockyadmin/.ssh
chmod -R go-rwx /home/rockyadmin/.ssh
cp wsl.conf /etc

